import torch.nn as nn
import torch
import numpy as np
from torch.nn import functional as F


class Interpolate(nn.Module):
    def __init__(self, scale_factor, mode):
        super(Interpolate, self).__init__()
        self.interp = torch.nn.functional.interpolate
        self.scale_factor = scale_factor
        self.mode = mode

    def forward(self, x):
        x = self.interp(x, scale_factor=self.scale_factor, mode=self.mode, align_corners=False)
        return x


class ResBlock1D(nn.Module):
    def __init__(self, channels):
        super(ResBlock1D, self).__init__()
        self.conv1 = torch.nn.Conv1d(channels, channels // 2, kernel_size=1, bias=False)
        self.bn1 = nn.BatchNorm1d(channels)
        self.relu = nn.ReLU(inplace=True)
        self.conv2 = torch.nn.Conv1d(channels // 2, channels, kernel_size=3, padding=1, bias=False)
        self.bn2 = nn.BatchNorm1d(channels // 2)

    def forward(self, x):
        identity = x

        out = self.bn1(x)
        out = self.relu(out)
        out = self.conv1(out)

        out = self.bn2(out)
        out = self.relu(out)
        out = self.conv2(out)

        out += identity
        return out


class Tract(torch.nn.Module):
    """
    Vocal tract
    """

    def __init__(self, n_mel_channels):
        super(Tract, self).__init__()
        self.tract_n = 44
        self.layers = 80
        self.n_mel_channels = n_mel_channels

        self.mel_to_admittance = torch.nn.Sequential()
        self.mel_to_admittance.add_module("conv_1", torch.nn.Conv1d(n_mel_channels, n_mel_channels * 2, 1))
        i = 2
        for r in range(10):
            self.mel_to_admittance.add_module("res1d_{}".format(i), ResBlock1D(n_mel_channels * 2))
            i += 1
        self.mel_to_admittance.add_module("relu_{}".format(i), torch.nn.ReLU())
        self.mel_to_admittance.add_module("upsample{}".format(i), torch.nn.ConvTranspose1d(n_mel_channels * 2,
                                                                                           n_mel_channels * 2,
                                                                                           2, stride=1))
        old_channels = n_mel_channels * 2
        for new_channels in [n_mel_channels * 2, n_mel_channels * 3 // 2, self.tract_n + 20]:
            self.mel_to_admittance.add_module("interpolate_{}".format(i), Interpolate(scale_factor=2, mode='linear'))
            self.mel_to_admittance.add_module("conv_{}".format(i),
                                              torch.nn.Conv1d(old_channels, new_channels, 4, padding=1))
            old_channels = new_channels
            i += 1

        self.mel_to_admittance.add_module("interpolate_{}".format(i), Interpolate(scale_factor=64, mode='linear'))
        self.mel_to_admittance.add_module("conv_{}".format(i), torch.nn.Conv1d(self.tract_n + 20, self.tract_n + 1, 4))
        self.mel_to_admittance.add_module("tanh_{}".format(i), torch.nn.Tanh())

        shift_left = torch.from_numpy(np.asarray([[0, 0, 1]])).float().to("cuda:0")
        self.shift_left = shift_left.view(1, 1, 1, 3)
        print("self.shift_left", self.shift_left)

        shift_right = torch.from_numpy(np.asarray([[1, 0, 0]])).float().to("cuda:0")
        self.shift_right = shift_right.view(1, 1, 1, 3)
        print("self.shift_right", self.shift_right)

        self.apply_padding_right_tract = torch.nn.ConstantPad2d((1, self.tract_n - 2, 0, 0), 0)
        self.apply_padding_left_tract = torch.nn.ConstantPad2d((0, self.tract_n - 1, 0, 0), 0)

    def forward(self, forward_input, train=False):
        audio, spect = forward_input
        # audio: [batch, seg_n]
        # spect: [batch, mel_channels, seg_n/250]

        audio = audio.unsqueeze(-1)

        batch_size, audio_length, _ = audio.size()
        admittance = self.mel_to_admittance(spect)
        admittance = admittance.permute(0, 2, 1)
        admittance = admittance.unsqueeze(1)

        assert (admittance.size(2) >= audio_length + self.tract_n + self.layers)
        if admittance.size(2) > audio_length + self.tract_n + self.layers:
            admittance = admittance[:, :, :audio_length + self.tract_n + self.layers]

        tract_r = self.apply_padding_right_tract(admittance[:, :, 0:audio_length, 1].unsqueeze(-1))
        tract_l = self.apply_padding_left_tract(admittance[:, :, 0:audio_length, 1].unsqueeze(-1))

        admittance_mtx = []
        for i in range(self.tract_n - 2):
            shifted_r = F.conv1d(tract_r, self.shift_right, bias=None, stride=1, padding=[0, 1])
            new_tract_r = shifted_r - (shifted_r + tract_l) * admittance[:, :, i + 1:i + 1 + audio.size(1),
                                                              :self.tract_n]

            shifted_l = F.conv1d(tract_l, self.shift_left, bias=None, stride=1, padding=[0, 1])
            new_tract_l = shifted_l + (shifted_l + tract_r) * admittance[:, :, i + 1:i + 1 + audio.size(1),
                                                              1:]
            tract_r = new_tract_r
            tract_l = new_tract_l

        admittance_mtx.append(tract_r[:, 0, :, -1].unsqueeze(-1))

        for r in range(self.layers):
            i = r + self.tract_n - 2
            shifted_r = F.conv1d(tract_r, self.shift_right, bias=None, stride=1, padding=[0, 1])
            new_tract_r = shifted_r - (shifted_r + tract_l) * admittance[:, :, i + 1:i + 1 + audio.size(1),
                                                              :self.tract_n]

            shifted_l = F.conv1d(tract_l, self.shift_left, bias=None, stride=1, padding=[0, 1])
            new_tract_l = shifted_l + (shifted_l + tract_r) * admittance[:, :, i + 1:i + 1 + audio.size(1),
                                                              1:]
            tract_r = new_tract_r
            tract_l = new_tract_l

            admittance_mtx.append(tract_r[:, 0, :, -1].unsqueeze(-1))

        admittance_mtx = torch.cat(admittance_mtx, -1)
        output_aduio_mtx = audio * admittance_mtx

        for i in range(self.layers + 1):
            if i == 0:
                output_aduio = output_aduio_mtx[:, :, 0]
            else:
                output_aduio[:, i:] += output_aduio_mtx[:, 0:audio_length - i, i]
        output_aduio = output_aduio.unsqueeze(1)
        # print(output_aduio.size())
        output_aduio = torch.nn.functional.avg_pool1d(output_aduio, 2, stride=None,
                                                      padding=0, ceil_mode=False,
                                                      count_include_pad=True)
        # print(output_aduio.size())
        output_aduio = output_aduio.squeeze()
        return output_aduio

    def get_admittance(self, spect, audio_length):
        admittance = self.mel_to_admittance(spect)
        admittance = admittance.permute(0, 2, 1)
        admittance = admittance[:, :, :audio_length + self.tract_n + self.layers]
        return admittance, audio_length + self.tract_n + self.layers

