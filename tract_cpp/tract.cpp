// Created by Bruno Englert
// All rights reserved.

#include <boost/python.hpp>
#include <boost/python/numpy.hpp>
#include "tract.h"

namespace bp = boost::python;
namespace np = boost::python::numpy;

using namespace boost::python;

BOOST_PYTHON_MODULE (tract) {
    class_<Tract>("Tract", init<>())
            .def("run", &Tract::run);
}