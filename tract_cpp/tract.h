// Created by Bruno Englert
// All rights reserved.

#include <iostream>
#include <cmath>
#include <boost/python.hpp>
#include <boost/python/numpy.hpp>
#include <boost/algorithm/clamp.hpp>

namespace p = boost::python;
namespace np = boost::python::numpy;


#ifndef TRACT_H
#define TRACT_H

class Tract {
private:
    static const int n = 44;
    float R[n] = {};
    float L[n] = {};
    float junctionOutputR[n+1] = {};
    float junctionOutputL[n+1] = {};
public:
    Tract() {
        Py_Initialize();
        np::initialize();
    }

    np::ndarray run(int length, np::ndarray input_audio, np::ndarray input_admittance) {
        float* output = new float[length/2]();
        float* arr_audio;
        float* arr_admittance;
        arr_audio = reinterpret_cast<float*>(input_audio.get_data());
        arr_admittance = reinterpret_cast<float*>(input_admittance.get_data());
        unsigned long jump = this->n + 1;
        unsigned long current_shift;
        for(unsigned long k=0; k<length; k++){
            current_shift = jump * k;
            this->junctionOutputR[0] =  this->L[0] * arr_admittance[current_shift] + arr_audio[k];
            this->junctionOutputL[this->n] = this->R[this->n - 1] * arr_admittance[k*jump+this->n];
            for (unsigned long i = 1; i < this->n; i++) {
                float w =  arr_admittance[current_shift+i] * (this->R[i - 1] + this->L[i]);
                this->junctionOutputR[i] = this->R[i - 1] - w;
                this->junctionOutputL[i] = this->L[i] + w;
            }

            for (int i = 0; i < this->n; i++) {
                this->R[i] = this->junctionOutputR[i] * 0.951;
                this->L[i] = this->junctionOutputL[i + 1] * 0.951;
                //this->R[i] = boost::algorithm::clamp(this->junctionOutputR[i], -1, 1);
                //this->L[i] = boost::algorithm::clamp(this->junctionOutputL[i+1], -1, 1);
            }

            output[k/2] += this->R[this->n - 1];
        }

        return np::from_data(output, np::dtype::get_builtin<float>(),
                             p::make_tuple(length/2),
                             p::make_tuple(sizeof(float)),
                             p::object());
    }
};

#endif /* TRACT_H */

