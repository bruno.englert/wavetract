import torch
from torch.nn import functional as F
from torch import nn


class PadWithin1D(nn.Module):
    def __init__(self, stride=2):
        super(PadWithin1D, self).__init__()
        self.stride = stride
        self.w = torch.zeros(self.stride).float().to("cuda:0")
        self.w[0] = 1

    def forward(self, feats, train=False):
        if train:
            return F.conv_transpose1d(feats,
                                      self.w.expand(feats.size(1), 1, self.stride),
                                      stride=self.stride, groups=feats.size(1))

        else:
            if not hasattr(self, 'w_inverse'):
                # Reverse computation
                W_inverse = self.w
                if feats.type() == 'torch.cuda.HalfTensor':
                    W_inverse = W_inverse.half()
                self.w_inverse = W_inverse

            return F.conv_transpose1d(feats,
                                      self.w_inverse.expand(feats.size(1), 1, self.stride),
                                      stride=self.stride, groups=feats.size(1))
