# WaveTract

This is a neural network based speech synthesiser. It has a [WaveGlow](https://github.com/NVIDIA/waveglow) part for low 
frequency prediction, which is upsampled by a digital waveguide.


This work is the result of Bruno Englert's BSc thesis. 

## Audio samples
https://soundcloud.com/user-688388606/sets/neural-network-based-vocoder