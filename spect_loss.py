import torch
from pylab import subplot, specgram, imshow, axis
import pylab

from mel2samp import MAX_WAV_VALUE
from mkdir import mkdir
from spectrogram import Spectrogram
import numpy as np
import matplotlib.pyplot as plt


class SpectLoss(torch.nn.Module):
    def __init__(self, model, sigma=1.0):
        super(SpectLoss, self).__init__()
        self.sigma = sigma
        self.model = model
        self.spectrogram = Spectrogram(n_fft=128, hop=64, normalize=False)
        self.train_wg = None
        self.train_waveglow(False)

        mkdir("spectrogram")

    def train_waveglow(self, val):
        if val is not self.train_wg:
            print("Training switched to \"train_waveglow: {}\"".format(val))
            self.train_wg = val

            for param in self.model.WN.parameters():
                param.requires_grad = self.train_wg
            for param in self.model.convinv.parameters():
                param.requires_grad = self.train_wg
            for param in self.model.upsample.parameters():
                param.requires_grad = self.train_wg

    def forward(self, model_output):
        target, pred_g_bef_up, pred_g, pred_t, iteration, noise= model_output

        # target = Loss3.norm(target)
        # pred_t = Loss3.norm(pred_t)

        spec_target = torch.log(torch.abs(self.spectrogram(target)+1e-7))
        spec_pred = torch.log(torch.abs(self.spectrogram(pred_t)+1e-7))

        if iteration % 1 == 0:
            plt.figure(figsize=(15, 20))
            fs = [22050, 8000, 22050, 22050, 22050]
            label = ["target", "glow before upsample", "glow after upsample", "waveguide", "noise", "diff", "pytorch target", "pytorch waveguide"]
            specgrams = []
            for i, k in enumerate([target, pred_g_bef_up, pred_g, pred_t, noise]):
                subplot(811 + i)#.set_title(label[i])
                # print(k.size())
                audio = (k.cpu().detach().numpy()[0] * MAX_WAV_VALUE).astype('int16')
                specgrams.append(specgram(audio, NFFT=128, Fs=fs[i], noverlap=64))
                # print(i, np.std(audio), np.average(audio))

            i += 1
            subplot(816)#.set_title(label[i])
            spec1, freqs, t, im = specgrams[0]
            spec2, freqs, t, im = specgrams[2]

            # print(spec1.shape)
            spec1 = np.abs(spec1) + 1e-7
            spec2 = np.abs(spec2) + 1e-7
            spec1 = np.log(spec1)
            spec2 = np.log(spec2)
            spect_diff = (spec1 - spec2)**2
            spect_diff = np.flipud(spect_diff)

            pad_xextent = (128. - 64.) / 22050. / 2.
            xextent = np.min(t) - pad_xextent, np.max(t) + pad_xextent

            xmin, xmax = xextent
            extent = xmin, xmax, freqs[0], freqs[-1]


            imshow(spect_diff, extent=extent)
            axis('auto')

            i += 1
            subplot(817)#.set_title(label[i])
            imshow(spec_target.cpu().detach().numpy()[0].T, extent=extent)
            axis('auto')


            i += 1
            subplot(818)#.set_title(label[i])
            imshow(spec_pred.cpu().detach().numpy()[0].T, extent=extent)
            axis('auto')

            #plt.tight_layout()

            pylab.savefig('spectrogram/foo{}.png'.format(iteration))
            plt.close('all')

        # audio_l2_loss = torch.sum((target - pred_t).pow(2)) / (target.size(0) * target.size(1))
        spec_l2_loss = torch.mean(((spec_target - spec_pred).pow(2)))
        audio_l1_loss = torch.mean(torch.abs(target - pred_t))

        return audio_l1_loss + spec_l2_loss

    @staticmethod
    def norm(x):
        return x / torch.sqrt(torch.nn.functional.relu(torch.sum(x * x)) + 1e-6).expand_as(x)
